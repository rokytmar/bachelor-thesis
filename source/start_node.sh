#/bin/bash
export PICOCLUSTER_SERVER=127.0.0.1:12345
export UDP_PACKET_SIZE=1100000
export END_SENT_MESSAGE_INFO=false
export ON_CLUSTER=false

java -jar project/picocluster/node/target/node-0.9-jar-with-dependencies.jar
