\chap Introduction

This project emerged from a discussion about simulating distributed algorithms in a non-distributed environment. Such simulation may achieve excellent results regarding solution correctness, but it can only partially substitute an actual distributed environment. Even though additional simulation settings may compensate for the lack of communication issues and networking delays, the hardware restrictions in limited thread count and thread management will never let the individual distributed subproblems be genuinely independent. The result difference can be enormous, and even algorithms that behave deterministically could end up unexpectedly. 

This bachelor's thesis aims to evaluate specific distributed algorithms experimentally and discuss their results from various simulated environments and truly distributed ones. 

To achieve this, I will introduce two selected distributed algorithms and integrate them into a platform created for running and managing tasks across a distributed environment. Extensive research and modifications will also be conducted in order to allow the platform to operate on a single CPU apart from the distributed network to collect data from simulated conditions.

Lastly, I will present various metrics to test the scaling of the algorithms in all deployment settings and experimentally evaluate and discuss the results collected.

\sec Assignment
\begitems
\style n
\medskip
* Analyze the features of the distributed computational platform for Raspberry-Pi cluster
\medskip
* Propose and implement an extension of the platform for validation of different algorithms deployment (single-CPU, multi-CPU, distributed experiments)
\medskip
* Implement selected distributed coordination multi-agent algorithms
\medskip
* Propose an evaluation metric to compare various features of implemented algorithms according to deployment strategy.
\medskip
* Conduct extensive experimental evaluation of the algorithms on personal computer and Raspberry-Pi cluster.

\chap Theoretical background
Distributed computing has seen a significant increase in utilization over the last few years. Multi-agent path planning, one of the possible usages of such algorithms, focuses on choosing optimal paths for multiple agents in complex environments while avoiding mutual collisions. The coordination problem was chosen as a typical example of a more advanced distributed algorithm with significant potential for practical usage and many possible solution approaches which can produce compelling results for the scaling evaluation.

Nevertheless, the implementation of such algorithms can vary significantly. Many modern algorithms are described only theoretically, and the potential implementations are often accomplished using a simulated distributed environment. Even though the difficulties of acquiring a truly distributed and well-behaving\fnote{e.g. easy to operate, troubleshoot, and resource undemanding} platform are an acceptable cause of these implementations, a question arises on how this simulation might influence the algorithm's runtime and results.

The main difference between the simulations and the actual distributed system is resource sharing between the client processes. Not only do they share RAM, but the demands and possible racing for CPU threads might result in a process having to wait for resources taken by another client. Furthermore, truly distributed systems introduce a delay in communication.

\label[pico-theory]
\sec Picocluster platform for distributed algorithms
The chosen platform for controlling distributed algorithms is the {\em Application for distributed algorithms on Picocluster}\cite[janků_2022], developed by Roman Janků. This application is specifically designed to manage tasks and assignments within a distributed system using a Picocluster\cite[picocluster_llc] architecture. The project comprises three modules: the Server, the Node, and the Library. Each module can be independently packaged as a .jar executable, with the server and node jars fully executable.

\begitems
* The "Library" module represents a shared codebase for the other two modules, containing essential code for communication, utility, and algorithms. 

* The "Node" module provides the necessary code to run an executable application on individual clients within the distributed system, managing algorithm execution, LED signals, and communication between nodes. 

* The "Server" module serves as the user interface for the application, managing the distribution and control of tasks. Additionally, this module manages the passing of task arguments, as well as the collection and export of resulting data.
\enditems

\medskip
\clabel[pico-server-gui]{Showase of a GUI of the server module of Picocluster platform}
\picw=15cm \cinspic imgs/gui-plain.png
\caption/f Showase of a GUI of the server module of Picocluster platform
\medskip

The platform provides the base for communication between the server and the clients using the "TCP" protocol and peer-to-peer communication for the clients over "UDP". Furthermore, it introduces a framework that allows the creation of various tasks that can be run across the clients and manage their runtime. The management includes assigning specific tasks to several clients, starting and stopping the task, and suspending or banishing clients. The framework also allows passing formatted parameters to each task and exporting the results.

The original implementation uses static ports for "TCP" and "UDP" communication, which requires starting each node on a unique IP address so the nodes can distinguish themselves and communicate. 

\label[raspi-cluster-theory]
\secc Raspberry Pi platform
\clabel[blinkt-pic]{Blinkt! module}
\picw=14cm \cinspic imgs/blinkt.jpg
\caption/f A "Blinkt! module". Image taken from \cite[janků_2022]
\medskip

Although the "Picocluster"\cite[janků_2022] platform can run tasks on any system consisting of separate machines connected to the same network, it has been specifically designed to run the nodes on a Raspberry Pi cluster. This cluster comprises individual Raspberry Pi machines, each equipped with a "Blinkt!"\urlnote{https://shop.pimoroni.com/products/blinkt?variant=22408658695} module connected to its "GPIO" header, allowing the programmer to visualize the algorithm's state via LED signals.

\medskip
\clabel[pico-big=photo]{48-node Pico cluster connected to PC}
\picw=14cm \cinspic imgs/big-connected.png
\caption/f 48-node Pico cluster connected to personal computer
\medskip

Two of these platforms were available for this thesis --- a "Pico 5" cluster comprising five and a big cluster with 48 individual "Raspberry Pi 4B+" boards. These single-board computers contain a 64-bit "ARM v7" processor with four cores and 8 GB ram\cite[janků_2022]. Full specifications of these clusters and Raspberry Pi boards can be found in Chapter 2 of Roman Janků's bachelor thesis\cite[janků_2022].

The individual boards' ethernet ports are connected to an internal switch, which is connected to the side of the cube and allows the connection to all included boards. The boards have a predefined static IP address and operate on network "10.1.10.0/24"\cite[janků_2022]. The scheme of connection can be seen in Figure \ref[pico-connections], and the step-by-step manual on connecting to the cluster can be found in Appendix A of the above-mentioned bachelor thesis of Mr. Janků.

\medskip
\clabel[pico-connections]{A scheme of network connection of Picocluster}
\picw=14cm \cinspic imgs/connections.png
\caption/f A scheme of network connection of the Raspberry Pi clusters. Image taken from \cite[janků_2022]
\medskip

\label[algo-theory]
\sec Algorithms
The multi-agent coordination problem was chosen as a typical example of a more advanced distributed algorithm with practical usage and many possible solution approaches, which can produce exciting results for the scaling evaluation. The selected algorithms are {\sbf ADPP} (Asynchronous Decentralized Priority Planning)\cite[cap_2015_b] and {\sbf COBRA} (Complete Best Response Approach)\cite[Čáp_Vokřínek_Kleiner_2015]. These algorithms were selected because they are implemented using the same libraries and were proposed, and both were developed by teams led by Dr. Michal Čáp\urlnote{https://michalcap.net/}. These facts allow easy integration of both algorithms. Moreover, they solve the problem using different approaches, resulting in more complex data.

An essential feature of these algorithms is that they guarantee a solution in a "well-formed" environment. This term stands for environments that guarantee an existing path between any free docks (e.g., start and goal positions), even if other agents occupy all other docks or there are planned trajectories between these nodes. The precise definition of this term can be found in section 4.3 of Michal Čáp's doctoral thesis\cite[cap_2016_thesis].

\medskip
\clabel[well-formed-scheme]{Well-formed infrastructure }
\picw=7cm \cinspic imgs/well-former.png
\caption/f Examples of well-formed and not well-formed infrastructure. Image taken from \cite[cap_2016_thesis]
\medskip

\label[adpp-theory]
\secc ADPP algorithm
The {\em Asynchronous Decentralized Priority Planning} algorithm\cite[cap_2015_b] was proposed by Michal Čáp, Peter Novák, Alexander Kleiner and Martin Selecký, and also solves multi-agent path-planning problems.

The ADPP algorithm comprises several vital steps. Firstly, the agents are initialized with their initial positions and goal locations. Then, priorities are assigned to the agents, which are determined by their names. The names assigned are unique and dependent on a numeric id, and the priorities are set alphabetically.

Subsequently, agents simultaneously plan their paths. Upon the planning completion, the agent sends a new message to all other agents, informing them that it has generated a new trajectory. Receiving this message from an agent with higher priority triggers collision detection, which may cause new trajectory planning. Each agent stores the trajectories of higher-priority agents and uses them as dynamic obstacles when planning new trajectory.

\clabel[adpp-worst-best]{Show}
\picw=7cm \cinspic imgs/adpp-best-vs-worst.png
\caption/f Comparison of the worst-case (on the left) and the best-case (on the right) scenarios of ADPP. The image was edited from a base from \cite[cap_2015_b]
\medskip

When the highest priority agent finishes planning, it also sends a message informing other agents that it has finished. When an agent has a non-conflicting trajectory and receives this message from an agent that is precisely one priority above, it also finishes. After the lowest priority agent finishes in this manner, it recognizes that it is the last one to do so and informs all other agents about a successful convergence, which ends the algorithm.

This approach more effectively exploits the advantages of a distributed system, as it allows multiple agents to plan simultaneously. The best-case scenario runtime is therefore based on the longest time of any of the agents' planning. In contrast, the worst-case scenario includes the lowest-priority agent to plan once for each other agent. A comparison of the runtime of these two scenarios can be seen in Figure \ref[adpp-worst-best].
\medskip
\clabel[adpp-vis]{Showcase of ADPP runtime visualization}
\picw=15cm \cinspic imgs/adpp-showcase.png
\caption/f Showcase of ADPP runtime visualization
\medskip

The Figure \ref[adpp-worst-best] also represents a rough comparison of the algorithm's runtime results in a distributed environment and on a limited count of single-CPU threads, which will always produce results resembling the worst-case scenario. The effect responsible for this resemblance is that only one agent can plan at a time.

\label[cobra-theory]
\secc COBRA algorithm
{\em Continuous Best-Response Approach (COBRA)}~\cite[Čáp_Vokřínek_Kleiner_2015] is a multi-agent path-planning algorithm proposed by Dr. Michal Čáp, doc. Jiří Vokřínek and doc. Alexander Kleiner in 2015. The algorithm introduces a {\em planning token}, which includes information about all currently planned trajectories, and whose possession is a necessity for an agent in order to plan his trajectory. Such an approach guarantees a solution in well-formed\cite[cap_2016_thesis] infrastructure. The original algorithm is implemented in Java, with a handful of testing instances and a visualization tool.

\medskip
\clabel[cobra-vis]{Showcase of COBRA runtime visualization}
\picw=15cm \cinspic imgs/cobra-showcase.png
\caption/f Showcase of COBRA runtime visualization
\medskip

Agents participating in this algorithm can create any number of tasks sequentially but will always have to obtain the token to plan. This is the cause of the algorithm's solution guarantee. There will be no conflicts between the trajectories as only one agent can plan at a time and possesses all currently planned trajectories. However, this also introduces a prolongation caused by the race for token possession and sequentializes the algorithm's runtime.

While talking about the algorithm's runtime, it is essential to note that it is implemented as a theoretical simulation. Each agent is run on a separate thread, with the token being implemented as a shared Singleton object, whose acquisition is controlled by a critical section. Furthermore, the time spent on a trajectory is simulated in real-time. This causes the algorithms' CPU requirements to be minimal during the runtime, as, for the most part, there is no planning, and the agents are only traveling.
