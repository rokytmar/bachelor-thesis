
\bibchap

\usebib/c (simple) mybase

\app Project structure and usage
The project is a mixture of three different subprojects, which are bundled inside the larger root project of this project.
The three subprojects are located in the "project" directory of the root project
\begitems
* {\sbf adpp-journal} --- a custom fork of the original repository of Dr. Čáp, containing the sources for the ADPP algorithm along with many others.
* {\sbf cobra-icaps2015} --- a custom fork of the original repository of Dr. Čáp containing sources for the COBRA algorithm and an implementation of the ORCA algorithm.
* {\sbf picocluster} --- a custom fork of a subtree of Roman Janků's repository containing his bachelor thesis.

\sec Version manager
The whole project uses Git as a version manager. The root repository contains sources for both the thesis, located in the "thesis" directory, and the project itself, located in the "source" directory. The project folder can, however, seem empty, as this directory's only content is in the form of "submodules". This approach allows the user to bundle three repository forks for each subproject listed above, which enables proper history management and content separation.

The following command allows cloning the whole project, including the submodules.

\begtt
git clone --recurse-submodules git@gitlab.fel.cvut.cz:rokytmar/
bachelor-thesis.git
\endtt

Additionally, the following commands can update the content of the submodules at any time.

\begtt
git submodule init
git submodule update
\endtt 

Both the ADPP and COBRA repositories are standard git Forks. However, the Picocluster's repository forking has proven to be quite challenging, as the repository also contains all of the sources for Roman Janků's bachelor thesis. I created a subtree of this repository, which only contains the folder with the desired application. The resulting forked repository, therefore, contains only desired folders, and the history is filtered to contain only commits that affect this subtree. The forked subtree repository is still linked to the upstream original, and merges can occur to exchange commits and git flow between the upstream and the fork.
Nevertheless, the user can work with all three submodules as with standard git repositories, with the only addition being the need to commit the submodule changes to the root project.


\sec Maven structure
Another common feature of all three subprojects is the usage of "Maven" as a dependency manager and packaging tool. This fact was used to bundle the projects further, creating a root "Maven" project. This project serves only as a parent wrapping all the other modules, allowing the whole project to be packaged from the "source" folder by running the "mvn package" command., which will create ".jar" files for all submodules.

The code snippet bellow provides a preview of the parent module's "pom.xml" 
\begtt
<groupId>cz.cvut.fel.rokytmar.picocluster-mapp</groupId>
<artifactId>root</artifactId>
<packaging>pom</packaging>
<version>0.1</version>
<modules>
    <module>project/picocluster</module>
    <module>project/cobra-icaps2015</module>
    <module>project/adpp-journal/admap-solver</module>
</modules>
\endtt

The Picocluster module further divides into three modules, one for each the "node", the "server", and the "library".

Additionally, the ADPP and COBRA modules were added as dependencies to all the modules of the Picocluster in order to allow the platform to access all respective packages needed to integrate the algorithms. This also allows the "node-0.9-jar-with-dependencies.jar" executable to carry all dependencies needed for any of the algorithms' run, thus reducing the number of files needed for a deploy to only one.

\sec Running the algorithms and experiment
In order to run either any of the algorithms or the experiment, we must first package the project using "mvn package" command and then start the server and the desired amount of nodes. 

\secc Starting the project and algorithms
To launch the server, we can simply run the {\sbf start_server.sh} script located in the root of the project's source directory.
.
The node has multiple possibilities for launching. We can either start the node on a cluster environment, which requires a machine with a "Blinkt!" module connected to its GPIO header, or on any available machine.

Either way, there are three possible methods how to start the node. 
The first includes launching the {\sbf start_node.sh} script in the source root. This is by far the easiest method, and all required environment variables can be set in the script. Ensure that the {\em PICOCLUSTER_SERVER} variable is set to correct the IP and port the server is running on.

Another option is to use Docker. There are two available Dockerfiles, called "Dockerfile" and "Dockerfile-noAnsible". 
The "Dockerfile" is, however, tailored specifically for the use on the cluster, using "rpi-base-image" as a base for the docker image and creating the image in an environment prepared by "Ansible" deployment pipeline created by Roman Janků. Therefore, the usage of "Dockerfile-noAnsible" is suggested. 

The environment variables are specified in the docker file, and the node can be created by this command:

\begtt
sudo docker build . --file Dockerfile-noAnsible -t picocluster-node
\endtt

Subsequently, this command can be used to run the node:

\begtt
sudo docker run picocluster-node
\endtt

Furthermore, the docker allows us to specify threads on which the node will be allowed to run. To achieve this, use a command similar to the following one, with the custom specification of the "cpuset":

\begtt
sudo docker run --rm --cpuset-cpus="1-4" picocluster-node
\endtt

Lastly, the already mentioned method using Ansible can be used. To launch the node with this setup, ensure your Raspberry Pi's IP address is listed in any group of the "hosts" file located in the source root. 
Ansible can be used to create various pipeline tasks on all specified machines. These tasks are located in the source root and are named "full-deploy.yml", "deploy.yml", "restart-docker.yml" and "redeploy-docker.yml". The effect of the tasks is apparent from their name.

To start any of these tasks, use the following command:
\begtt
ansible-playbook -i ./hosts -kK --ssh-extra-args='-o 
"PubkeyAuthentication=no"' -u picocluster deploy.yml
\endtt

These scripts can build the docker image from scratch, or just redeploy it, and will always start the node right at the end of the task.

Starting any of the algorithms is then relatively easy, as it only requires a sufficient amount of nodes to be connected and the arguments inputted correctly. However, the user can also provide no input, and the server will use default arguments.

\secc Starting the experiment
Starting the experiment is even easier than starting one node, as there is only one parameter to be inputted. There are however some other problems, a user must know of.

The experiment runs all the desired algorithms on all instances it can find. If there are no instances generated, start the "prepare-picoagents-experiment.sh" script in the "adpp-journal" folder for ADPP, or in the "cobra-icaps2015" folder for COBRA.

We must only be aware, to start the experiment task using all possible agents connected, to prevent multiple instances trying to run at once. Furthermore, starting the visualization slows down the experiment, and the visualization cannot be shut down, as it would shut down the entire "Server".

The results of the experiment will be stored in "data.out" file in each instance environment folder. Subsequent usage of "prepare-picoagents-results.sh" will result in "data.out.head" file beaing produced, containing the result data with a header, and usage of "prepare-picoagents-plots.sh" will generate plots from these data in "plots" folder.

Beware that running the experiment will always rewrite the whole output file, so in order to join the results of more experiment runs, we create backups and combine the files later using an editor or terminal.

\medskip

A thorough manual for launching the project on the cluster can be found in Appendix A of Roman Janků's bachelor thesis.

\label[app-attachments]
\app Attachments
All additional attachments have been uploaded to the KOS submission system and are also available in the project's GitLab repository\urlnote{https://gitlab.fel.cvut.cz/rokytmar/bachelor-thesis}. There are the following attachments:

\medskip

\begitems
* all the plots generated using the experiment results for both algorithms in all environments. These can be found in the "plots" folder of the uploaded attachments or at 

\url{https://gitlab.fel.cvut.cz/rokytmar/bachelor-thesis/-/tree/main/thesis/plots}.
\medskip
* the experiment results in a raw format. The data can be easily imported as a "CSV", and are located in the "results" folder of the uploaded attachments or at 

\url{https://gitlab.fel.cvut.cz/rokytmar/bachelor-thesis/-/tree/main/thesis/results}
\medskip
* the project's complete source code can be found in the "source" folder of the uploaded attachments or at 

\url{https://gitlab.fel.cvut.cz/rokytmar/bachelor-thesis/-/tree/main/source}.
\enditems
