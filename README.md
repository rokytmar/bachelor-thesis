Bachelor thesis
=================

**Name:** Experimental validation of distributed algorithms
**Author:** Martin Rokyta (rokytmar@fel.cvut.cz)  
**Supervisor:** doc. Ing. Jiří Vokřínek, Ph.D. (jiri.vokrinek@fel.cvut.cz)  
**Department of supervisor:** Department of Computer Science

About
------
This repository contains a bachelor thesis of Martin Rokyta, made in the summer semester of 2023 under supervision of Jiří Vokřínek.
The thesis refactors two distributed algorithms, implemented as a simulation for one machine, to be able to run on a truly distributed system and to evaluate and compare their performance on both environments experimentally. The algorithms selected for this task are the Continuous Best-Response Ap- proach algorithm (COBRA) and the Asynchronous Decentralized Priority Planning (ADPP), examples of multi- agent distributed coordination problem solvers. 

The algorithms were integrated into a platform for distributed computing on Raspberry Pi cluster, which was extended to run on any environment. The thesis provides an extensive eval- uation of the scaling of these algorithms on various deployment environments and discusses the advantages and dis advantages of different methods of distributed algorithm testing.

Structure
----------
This repository consists of two parts. The `thesis` folder includes all materials for the thesis itself, along with generated plots and , while the `source` folder consists of the actual code and resources. The source includes git submodules, each referring to a custom fork of one of the original projects.
It also wraps the subprojects into one maven project, using them as maven's modules.

Gitlab web interface allows to access the submodule repositories by clicking on the submodule placeholder in `source/projects` folder.

Cloning
---------
Due to the project structure, use `git clone --recurse-submodules git@gitlab.fel.cvut.cz:rokytmar/bachelor-thesis.git` to clone the whole project

To update the submodules later, following commands can be used

````
git submodule init
git submodule update
````

Packaging and starting
----------------------
Due to the structure, it is advised that the project is packaged by calling `mvn package` in the `project` directory, and to start each generated jar from this directory as well.
In case of IDE usage, be sure to set the working directory to source folder in run configuration.

The easiest way of starting the project includes starting the server using `./start_server.sh` script in the `source` folder, and starting any desired number of nodes using `start_node.sh`.
More advanced possibilities of starting the nodes are discussed in the thesis.

Experiment
-------------------------------
To generate the instances for the experiment, each of the `cobra-icaps2015` and `admap-journal` folders include `prepare-picoagents-experiment.sh` script, which will generate all instances appropriately, and can be further edited to include desired suffix to the input and output data

The experiment is run by starting the `ADPPExperiment` or `COBRAExperiment` task from the server's GUI. 

To generate the plots, first run the `prepare-picoagents-results.sh` script in each respective algorithm folder. This will add a head to the resuls output. Beware that this can overwrite previous experiment data.
Subsequently, run `prepare-picoagents-plots.sh` script in the directories, which will generate all supported plots.
